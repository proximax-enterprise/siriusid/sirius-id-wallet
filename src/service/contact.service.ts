import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ContactService {

  dataReceived;
  contactList = [];
  contactDetailIndex: number;

  observableIsOnline: BehaviorSubject<string>;
  contactReceived: null;

  constructor(
    private storage: Storage
  ) {
    this.observableIsOnline = new BehaviorSubject<string> (this.contactReceived);
    this.storage.get('contactList').then( val => {
      if(val){
        this.contactList = val;
      }
    })
  }

  addContact(name:string){
    let obj = {
      name: name,
      address: this.dataReceived
    }
    this.contactList.push(obj);
    this.storage.set("contactList",this.contactList);
  }

  removeContact(index:number){
    this.contactList.splice(index,1);
    this.storage.set('contactList',this.contactList);
  }
}
