import { TestBed } from '@angular/core/testing';

import { SiriusDidService } from './sirius-did.service';

describe('SiriusDidService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SiriusDidService = TestBed.get(SiriusDidService);
    expect(service).toBeTruthy();
  });
});
