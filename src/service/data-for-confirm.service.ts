import { Injectable } from '@angular/core';
import { MessageReceived } from 'siriusid-sdk';

@Injectable({
  providedIn: 'root'
})
export class DataForConfirmService {
  /**
   * Data variables
   */
  messageReceived : MessageReceived; // save data from QR code or URL link
  messageType; // check type of message received from QR code or URL link
  successTx : boolean; // check if transfer transaction success or not
  transaction; // save transaction parser
  /**
   * Configure block chain TEST_NET
   */
  //apiNode = "https://bctestnet1.xpxsirius.io"; 
  // apiNode = "https://bctestnet2.brimstone.xpxsirius.io";
  // NETWORK_TYPE = 'TEST_NET';

  /**
   * Configure for local host
   */
  // apiNode = "http://192.168.1.23:3000"; 
  // NETWORK_TYPE = 'MIJIN_TEST';

  constructor() { }
}
