import { TestBed } from '@angular/core/testing';
import { IonicStorageModule } from '@ionic/storage';

import { StoredAccountService } from './stored-account.service';

describe('StoredAccountService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      IonicStorageModule.forRoot()
    ],
    providers: []
  }));

  it('should be created', () => {
    const service: StoredAccountService = TestBed.get(StoredAccountService);
    expect(service).toBeTruthy();
  });
});
