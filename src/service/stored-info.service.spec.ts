import { TestBed } from '@angular/core/testing';

import { StoredInfoService } from './stored-info.service';
import { IonicStorageModule } from '@ionic/storage';

describe('StoredInfoService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      IonicStorageModule.forRoot()
    ]
  }));

  it('should be created', () => {
    const service: StoredInfoService = TestBed.get(StoredInfoService);
    expect(service).toBeTruthy();
  });
});
