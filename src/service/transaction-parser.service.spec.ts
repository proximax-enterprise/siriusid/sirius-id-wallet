import { TestBed } from '@angular/core/testing';

import { TransactionParserService } from './transaction-parser.service';
import { MessageReceived, TransactionRequestMessage } from 'siriusid-sdk';
import { PublicAccount, NetworkType, Address, MosaicId, NamespaceId, UInt64, Mosaic, TransferTransaction, Deadline, PlainMessage, AggregateTransaction, RawUInt64, NetworkCurrencyMosaic } from 'tsjs-xpx-chain-sdk';
import { DateTimeFormatter, Duration } from 'js-joda';

describe('TransactionParserService', () => {
  let service: TransactionParserService;
  beforeEach(() => {
    TestBed.configureTestingModule({})

    service = TestBed.get(TransactionParserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('transfer', async ()=>{

    // Make deadline
    const deadline = Deadline.create()
    const t = new Date(deadline.value.toString()).getTime()    
    const first = Deadline.timestampNemesisBlock*1000
    const ff = new UInt64(RawUInt64.fromUint((t - first)));
    const res = [ff.lower,ff.higher]

    // Make transaction
    var recipient = PublicAccount.createFromPublicKey('0123456789012345678901234567890123456789012345678901234567890123', NetworkType.MIJIN_TEST);
    var mosaic = NetworkCurrencyMosaic.createRelative(10);
    var tx = TransferTransaction.create(deadline, recipient.address, [mosaic], PlainMessage.create('Hello!'), NetworkType.MIJIN_TEST);
    var TxMessage = new TransactionRequestMessage('', tx, '');      

    const messageReceived = new MessageReceived(JSON.stringify(TxMessage))
    const result = await TransactionParserService.parser(messageReceived.message.payload.transaction)

    // Expect
    const expect_result = {
      "transaction":{
        "type":16724,
        "networkType":144,
        "version":-1879048189,
        "maxFee":[43250,0],
        "deadline": res,
        "signature":"","recipient":{
          "address":"SCOADJLK3UWYW4E2SN6WNWKB4DGLQHMTE3B4PHYE",
          "networkType":144
        },
        "mosaics":[{
          "amount":[10000000,0],
          "id":[3294802500,2243684972]
        }],
        "message":{
          "type":0,
          "payload":"48656c6c6f21"
        }
      }
    }

    expect(JSON.stringify(result)).toEqual(JSON.stringify(expect_result))


  })


  it('aggregate_completed', async () => {

    // Make deadline
    const deadline = Deadline.create()
    const t = new Date(deadline.value.toString()).getTime()    
    const first = Deadline.timestampNemesisBlock*1000
    const ff = new UInt64(RawUInt64.fromUint((t - first)));
    const res = [ff.lower,ff.higher]

    // Make transaction  
    let publicAccount = PublicAccount.createFromPublicKey("990585BBB7C97BB61D90410B67552D82D30738994BA7CF2B1041D1E0A6E4169B",NetworkType.TEST_NET);

    const publicKey1 = '803BD90020E0BB5F0B03AC75C86056A4D4AB5940F2A3A520694D8E7FF217E961';
    const recipient1 = Address.createFromPublicKey(publicKey1,NetworkType.TEST_NET);
    
    const namespaceId = new NamespaceId('prx.xpx');
    
    const amount = new UInt64(RawUInt64.fromUint(1))
    const mosaic1 = new Mosaic(namespaceId,amount);
    
    const tx1 = TransferTransaction.create(
      deadline,
      recipient1,
      [mosaic1],
      PlainMessage.create('This is message'),
      NetworkType.TEST_NET,
      new UInt64([0,0])
    );

    const pubAccount1 = PublicAccount.createFromPublicKey(publicKey1,NetworkType.TEST_NET);


    const tx = AggregateTransaction.createComplete(
      deadline,
      [tx1.toAggregate(publicAccount)],
      NetworkType.TEST_NET,
      [],
      new UInt64([0,0])
    )


    let TxMessage = new TransactionRequestMessage('hellu',tx,'');    
    console.log('tx message', TxMessage);


    const messageReceived = new MessageReceived(JSON.stringify(TxMessage))
    const result = await TransactionParserService.parser(messageReceived.message.payload.transaction)
    
    console.log('result ', JSON.stringify(result))


    // Expect
    const expect_result = {
      "transaction":{
        "type":16705,
        "networkType":168,
        "version":-1476395006,
        "maxFee":[0,0],
        "deadline":res,
        "signature":"",
        "transactions":[{
          "transaction":{
            "type":16724,
            "networkType":168,
            "version":-1476395005,
            "maxFee":[0,0],
            "deadline":res,
            "signature":"",
            "signer":"990585BBB7C97BB61D90410B67552D82D30738994BA7CF2B1041D1E0A6E4169B",
            "recipient":{
              "address":"VCTSYT3SPBID36GQDZRC3E4XOUQGIGF5CG6EQXRT",
              "networkType":168
            },
            "mosaics":[{
              "amount":[1,0],
              "id":[2434186742,3220914849]
            }],
            "message":{
              "type":0,
              "payload":"54686973206973206d657373616765"
            }
          }
        }],
        "cosignatures":[]
      }
    }
    


    
    expect(JSON.stringify(result)).toEqual(JSON.stringify(expect_result))

  })


  it('checkNamespaceId', async () => {
    const namespaceId = [2434186742,3220914849]

    const result = await TransactionParserService.checkNamespaceId(namespaceId)
    console.log('check namespace ',result)

    expect(result).toEqual(true)
  })


});
