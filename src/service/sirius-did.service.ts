import { Injectable } from '@angular/core';
import {StoredAccountService} from './stored-account.service';
import {ApiNodeService} from './api-node.service';
import {StoredInfoService} from './stored-info.service';
import { Storage } from '@ionic/storage';
import { SiriusChainProvider, IpfsStorageProvider, SiriusId, createDid, toNetworkIdentifier, SiriusPublicProfile, ServiceSection, DidDocument, IDidDocument, Identity, StreamHelper } from 'tsjs-did-siriusid';
import { MetadataHttp, PublicAccount, EncryptedMessage, PlainMessage, Account, NetworkType } from 'tsjs-xpx-chain-sdk';
import { IpfsConnection, BlockchainNetworkConnection, Protocol, ConnectionConfig, Uploader, StringParameterData, UploadParameter, BlockchainNetworkType } from 'tsjs-chain-xipfs-sdk';
@Injectable({
  providedIn: 'root'
})
export class SiriusDidService {

  
  static ipfsStorageProvider = "https://ipfs1-dev.xpxsirius.io:5443"; //default storage provider

  static storageProvider = new IpfsStorageProvider(
    SiriusDidService.ipfsStorageProvider
  );

  static siriusChainProvider;

  static registryProvider;

  static didDocument;

  static didHash;

  static storage: Storage

  static storedInfoService: StoredInfoService;

  initialize = false;
  constructor(
    private storedInfoService:StoredInfoService,
    private storage: Storage
  ) {
    SiriusDidService.storage = this.storage;
    SiriusDidService.storedInfoService = this.storedInfoService;
  }

  init(){
    console.log("SiriusDidService init");
    if (!this.initialize){
      this.storage.get("didDocument").then(val => {
        if (val){
          console.log("did was storaged in local");
          SiriusDidService.didDocument = DidDocument.fromJSON(val);
          console.log(SiriusDidService.didDocument);
        }
        else{
          SiriusDidService.recoveryDid();
        }
      }) 
    }
  }

  /**
   * Check this account have alreday did? If yes then get this did. If not then created new did
   */
  static async recoveryDid(){
    console.log("recoveryDid");
    let interval = setInterval(() => {
      if (ApiNodeService.apiNode){
        console.log("Api Node already setup " + ApiNodeService.apiNode);
        SiriusDidService.siriusChainProvider = ApiNodeService.apiNode;
        SiriusDidService.registryProvider = new SiriusChainProvider(
          SiriusDidService.siriusChainProvider
        );

        SiriusDidService.checkDidExist().then(result => {
          //Don't have did
          if (!result){
            this.createNewDid();
          }
          else{ 
            this.getDid();
          }
        })

        clearInterval(interval);
      }
      else {
        console.log("Api Node not working");
        console.log(".");
      }
    },1000);
  }

  static async createNewDid(){
    console.log("create new did");
    const siriusId = await SiriusId.createInstance(this.registryProvider, this.storageProvider);

    const publicProfile = {
      name : this.storedInfoService.getName(),
      email : this.storedInfoService.getEmail(),
      country : this.storedInfoService.getCountry(),
      phoneNumber : this.storedInfoService.getPhoneNumber()
    };

    // store in ipfs
    const profileContent = JSON.stringify(publicProfile);
    console.log("profileContent is:",profileContent);
    const docStream = StreamHelper.string2Stream(profileContent);

    const publicProfileResource = await this.storageProvider.save(docStream);
    console.log('Storage is online: ',await this.storageProvider.isOnline());

    const identity: Identity = await siriusId.create(StoredAccountService.getPrivateKey(), document => {
      document.addServiceSection(
        ServiceSection.createPublicProfileService(document.did, publicProfileResource),
      );
    });

    this.didDocument = identity.didDocument;

    SiriusDidService.storageLocalDidDocument();
    console.log(">>>>>>>>>>>>>");
    console.log(this.didDocument);
    console.log("<<<<<<<<<<<<<");
  }

  static async checkDidExist(){
    return new Promise(resolve => {
      const metadataHttp = new MetadataHttp(ApiNodeService.apiNode);
      metadataHttp.getAccountMetadata(StoredAccountService.getAddress()).subscribe(result => {
        if (result && result.fields.length > 1){ 
          console.log("Co did document rooi nhaaa");
          console.log(result);
          resolve (this.checkFormatDidField(result.fields));
        }
        else {
          console.log("Did document haven't been created");
          resolve(false);
        }
      }, error => {
        console.log("Metadata didn't init");
        resolve(false);
      });
    })
  }

  static async getDid(){
    console.log("get Did");

    console.log(this.registryProvider);
    console.log(this.storageProvider);
    const siriusId = await SiriusId.createInstance(this.registryProvider, this.storageProvider);
    const did = createDid({
      network: toNetworkIdentifier(ApiNodeService.NETWORK_TYPE),
      address: StoredAccountService.getAddress(),
    });
    console.log(did);
    this.didDocument = await siriusId.resolve(did);
    SiriusDidService.storageLocalDidDocument();
    console.log(this.didDocument);
    return this.didDocument;
    
  }

  static checkFormatDidField(fields:Array<object>){
    console.log("checkFormatDidField");
    const tmp = fields.filter(el => el['key'] == "didHash");
    const tmpURL = fields.filter(el => el['key'] == "didUrl");
    console.log(tmp);
    if (tmp.length == 1 && tmpURL.length == 1){
      SiriusDidService.didHash = tmp[0]['value'];
      return true;
    }
    else return false;
  }

  static storageLocalDidDocument(){
    SiriusDidService.storage.set('didDocument',this.didDocument.toJSON());
  }

  static convertApiNode(){
    let url;
    const lastCharacter = ApiNodeService.apiNode[ApiNodeService.apiNode.length-1];
    if (lastCharacter == "/"){
        url = ApiNodeService.apiNode.slice(0,ApiNodeService.apiNode.length-1);
    }
    else url = ApiNodeService.apiNode;

    const httpsIs = url.indexOf('https');
    let apiProtocol;
    let apiPort;
    let apiDomain;
    if (httpsIs == -1){
        apiProtocol = Protocol.HTTP;
        if (url.indexOf(':') > -1){
            apiDomain = url.slice(7,url.indexOf(':'));
            apiPort = Number(url.slice(url.indexOf(':')+1));
        }
        else {
            apiDomain = url.slice(7);
            apiPort = 80;
        }
    }
    else {
        apiProtocol = Protocol.HTTPS;
        apiPort = 443;
        apiDomain = url.slice(8);
    }
    console.log("=====================");
    console.log(apiProtocol + " " + apiDomain + " " + apiPort);
    console.log("=====================");
    return {
        'apiDomain' : apiDomain,
        'apiPort' : apiPort,
        'apiProtocol' : apiProtocol
    }
  }

  static async updateUserProfile(){
    console.log("attach user profile");

    // create the public profile resource
    const publicProfile = {
      name: SiriusDidService.storedInfoService.name,
      email: SiriusDidService.storedInfoService.email,
      country: SiriusDidService.storedInfoService.country,
      phoneNumber: SiriusDidService.storedInfoService.phoneNumber,
      avatar: SiriusDidService.storedInfoService.avatar
    }
    console.log("----------------------------------------");
    console.log(publicProfile);
    console.log("----------------------------------------");
    
    const profileContent = JSON.stringify(publicProfile);
    console.log("profileContent is:",profileContent);
    const docStream = StreamHelper.string2Stream(profileContent);

    const publicProfileResource = await this.storageProvider.save(docStream);
    console.log('Storage is online: ',await this.storageProvider.isOnline());
    const siriusId = await SiriusId.createInstance(this.registryProvider, this.storageProvider);

    const identity: Identity = await siriusId.update(StoredAccountService.getPrivateKey(), document => {
      document.clearServiceEndpoint();
      document.addServiceSection(
        ServiceSection.createPublicProfileService(document.did, publicProfileResource),
      );
    });
  }

  updateRegistryProvider(){
    return SiriusDidService.registryProvider = new SiriusChainProvider(
      ApiNodeService.apiNode
    );
  }

  updateStorageProvider(){
    //TO DO
  }

  static convertNetworkType(){
    switch (ApiNodeService.NETWORK_TYPE){
        case NetworkType.MAIN_NET: 
            return BlockchainNetworkType.MAIN_NET;
        case NetworkType.TEST_NET: 
            return BlockchainNetworkType.TEST_NET;
        case NetworkType.MIJIN: 
            return BlockchainNetworkType.MIJIN;
        case NetworkType.MIJIN_TEST: 
            return BlockchainNetworkType.MIJIN_TEST;
        case NetworkType.PRIVATE: 
            return BlockchainNetworkType.PRIVATE;
        case NetworkType.PRIVATE_TEST: 
            return BlockchainNetworkType.PRIVATE_TEST;
    }
  }
}
