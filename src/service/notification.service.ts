import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Listener, Address, TransferTransaction, EncryptedMessage, PublicAccount } from 'tsjs-xpx-chain-sdk';
import { MessageType } from 'siriusid-sdk';
import { ApiNodeService } from './api-node.service';
import { StoredAccountService } from './stored-account.service';
import { Subscription } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  notification: any;
  alert: any;

  subscribe: Subscription;
  listener: Listener;
  connect = false;
  ws: string;
  interval: NodeJS.Timeout;

  listNotifications = new Array();
  numberNoti = 0;
  confirmCredential = false;
  transaction: TransferTransaction;

  constructor(private storage: Storage) {}


  openConnectionWs() {
    this.storage.get('listNotifications').then(value => {
      if (value) {
        this.listNotifications = value;
      }
      this.updateNumberNoti();
    });

    this.updateWs();
  }

  async waiting() {
    this.connection();
    this.interval = setInterval(async () => {
      const listenerAsAny = this.listener as any;
      console.log(listenerAsAny.webSocket.readyState);
      if (listenerAsAny.webSocket.readyState !== 1) {
        this.listener = new Listener(this.ws, WebSocket);
        this.connection();
      }
    }, 1000);
  }

  async connection() {
    this.listener.open().then(() => {
      console.log(StoredAccountService.address);
      const address = Address.createFromRawAddress(StoredAccountService.address);
      try {
        this.subscribe = this.listener.confirmed(address).subscribe(transaction => {
          console.log('txn ------>', transaction);
          const transferTx = transaction as TransferTransaction;
          if (transferTx.message && transferTx.message.type == 1) { // encrypted message
            const encMess = EncryptedMessage.createFromPayload(transferTx.message.payload);
            const decMess = EncryptedMessage.decrypt(encMess, StoredAccountService.getPrivateKey(), transferTx.signer);
            const payload = JSON.parse(decMess.payload);
            if (payload.type == MessageType.CREDENTIAL_REQ) {
              this.listNotifications.push(payload.payload);
              this.updateNumberNoti();
              this.updateListNotification();
              console.log(payload);
              console.log(this.listNotifications);
            }
          }
        });
      } catch (error) {
        console.log('Has ocurred a error in connection with websocket!');
      }
    });
  }

  async updateWs() {
    if (this.interval) {
      clearInterval(this.interval);
      this.listener.close();
    }

    // waiting setting node done
    const interval = setInterval(() => {
      console.log('ApiNodeService', ApiNodeService.apiNode);
      if (ApiNodeService.apiNode) {
        this.ws = ApiNodeService.apiNode.replace('https', 'wss').replace('http', 'ws');
        console.log("-------------------");
        console.log(ApiNodeService.apiNode);
        console.log(this.ws);
        console.log("-------------------");
        // if (ApiNodeService.apiNode.indexOf('https://') > -1) {
        //   this.ws = 'wss://' + ApiNodeService.apiNode.slice(8, ApiNodeService.apiNode.length);
        // } else if (ApiNodeService.apiNode.indexOf('http://') > -1) {
        //   this.ws = 'ws://' + ApiNodeService.apiNode.slice(7, ApiNodeService.apiNode.length);
        // }

        ApiNodeService.checkNodeLive(ApiNodeService.apiNode).then(value => {
          console.log('node checked ---> ', value);
          this.connect = value;
          this.listener = new Listener(this.ws, WebSocket);
          this.connection();
          // this.waiting();
        });

        clearInterval(interval);
      }

      console.log(`Couldn't connect to websocket?`);
    }, 5000);
  }

  updateNumberNoti() {
    this.numberNoti = this.listNotifications.length;
  }

  updateListNotification() {
    this.storage.set('listNotifications', this.listNotifications);
  }
}
