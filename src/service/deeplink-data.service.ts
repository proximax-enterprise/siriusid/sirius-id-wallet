import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DeeplinkDataService {
  observableIsOnline: BehaviorSubject<string>;
  messaggeReceived: null;
  constructor() {
    this.observableIsOnline = new BehaviorSubject<string>(this.messaggeReceived);
  }
}
