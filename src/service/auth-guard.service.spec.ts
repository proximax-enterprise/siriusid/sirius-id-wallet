import { TestBed } from '@angular/core/testing';

import { AuthGuardService } from './auth-guard.service';
import { Router } from '@angular/router';
class MockRouter{
  navigateByUrl(url: string) { return url; }
}
describe('AuthGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      AuthGuardService,
      { provide: Router, useClass: MockRouter }
    ]
  }));

  it('should be created', () => {
    const service: AuthGuardService = TestBed.get(AuthGuardService);
    expect(service).toBeTruthy();
  });
});
