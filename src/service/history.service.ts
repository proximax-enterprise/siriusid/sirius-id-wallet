import { Injectable } from '@angular/core';
import { 
  NetworkType, 
  AccountHttp, 
  PublicAccount,
  TransferTransaction, 
  Transaction,
  Address,
  Order,
  TransactionType
} from 'tsjs-xpx-chain-sdk';

import { GetCoinNameService } from 'src/service/get-coin-name.service';
import { TransactionParserService } from 'src/service/transaction-parser.service';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  type: any;
  recepient: any;
  signer: any;
  dateTime: any;
  i: any;
  mosaic: any;
  message: any;
  coinName: any;

  /**
  * property length of transfer transactions array using in SiriusID project
  */
  lengthTx = 0;
  constructor() {}
  /**
   * Get confirmed transactions using in SiriusID project
   * @param publicKey 
   * @param api_url 
   * @param networkType 
   */
  getConfirmedTransaction (publicKey: string, api_url: string, networkType: NetworkType){
    const accountHttp = new AccountHttp(api_url);
    const publicAccount = PublicAccount.createFromPublicKey(publicKey, networkType);
    return new Promise<Transaction[]> ( res => {
      accountHttp.outgoingTransactions(publicAccount, {
        pageSize: 100,
        order: Order.DESC
      }).subscribe(tx => {
        // assign all tx[] that is transfer transaction
        var lengthTx = tx.length;
        var length = 0;
        var newArrayTx = [];
        for (var i = 0; i <lengthTx; i++){
          if (tx[i].type == TransactionType.TRANSFER){
            newArrayTx[length] = tx[i];
            length ++;
          }
        }
        this.lengthTx = length;
        res(newArrayTx);
      }, error => {
        console.error(error);
      }, () => {
        console.log('done.');
      }); 
    },)
  }
  /**
   * Get type of message: LOGIN, CONFIRM.
   * @param transaction 
   */
  getMessageType(transaction: any){
    const tx = transaction as TransferTransaction;
    if (tx.message.payload.includes('"messageType":1')){
      return 1;
    }
    else return 2;
  }
  /**
   * Get address of recipient
   * @param transaction 
   */
  getAddressRecipient(transaction: any){
    const transferTx = transaction as TransferTransaction;
    const add = transferTx.recipient as Address;
    return add.pretty();
  }
  /**
   * Get address of signer
   * @param transaction 
   */
  getAdressSigner(transaction: any){
    const tx = transaction as Transaction;
    const add =  tx.signer as PublicAccount;
    return add.address.pretty();
  } 
  /**
   * Get date, time of a transaction
   * @param transaction 
   */
  getDateTime(transaction: any){
    const tx = transaction as Transaction;
    const month = tx.deadline.value.month().name();
    const day = tx.deadline.value.dayOfMonth().toString();
    const year = tx.deadline.value.year().toString();
    const hour = tx.deadline.value.hour().toString();
    const min = tx.deadline.value.minute().toString();
    return month + ' ' + day + ' ' + year + ' ' + hour + 'h' + min;
  }
  /**
   * 
   * @param transaction 
   */
  getMosaicSent(transaction: any){
    const tx = transaction as TransferTransaction;

    if (tx.mosaics[0] != undefined){
      const a = tx.mosaics[0].amount.toHex();
      parseInt('0x' + a);
      return parseInt('0x' + a);
    }
    return 0;
  }

  async getCoinName(transaction: any){
    const tx = transaction as TransferTransaction;
    if (tx.mosaics.length > 0){
      let id = tx.mosaics[0].id;
      await GetCoinNameService.getCoinName(id);
      return GetCoinNameService.coin_name;
    }
    else return "";
  }
  /**
   * 
   * @param transaction 
   */
  getMessage(transaction: any){
    const tx = transaction as TransferTransaction;
    if (this.getMessageType(tx)== 2){
      return tx.message.payload;
    }
    return 'no message'
  }

  // private messagePayload (tx: any){
  //   const transferTx = tx as TransferTransaction;
  //   const json = JSON.parse(transferTx.message.payload);
  //   return json;
  // }
}
