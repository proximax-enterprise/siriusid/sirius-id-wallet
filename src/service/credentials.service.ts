import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { CredentialStored } from 'siriusid-sdk';
import { Account, ModifyAccountMetadataTransactionBuilder, NetworkHttp, MetadataHttp, BlockHttp, TransactionHttp, Deadline, MetadataModification, MetadataModificationType, FeeCalculationStrategy, NetworkType, PublicAccount } from 'tsjs-xpx-chain-sdk';
import { StoredAccountService } from './stored-account.service';
import { ApiNodeService } from './api-node.service';
import { IpfsConnection, Protocol, ConnectionConfig, Uploader, BlockchainNetworkConnection, UploadParameter, Downloader, DownloadParameter } from 'tsjs-chain-xipfs-sdk';

@Injectable({
  providedIn: 'root'
})
export class CredentialsService {

  private networkHttp: NetworkHttp;
  private metadataHttp: MetadataHttp;
  private blockHttp: BlockHttp;
  private transactionHttp: TransactionHttp;

  notEnoughArray;

  credentialsReq: any[] = new Array();

  credentials: any[] = new Array();

  credentialStoredList: any[] = new Array();

  credentialDetailIndex: number;

  credentialGroupIndex: number;

  currentCredential: any;

  credentialGroup:any[] = new Array();

  credentialSingle:any[] = new Array();

  groupRoute = false;

  start = false;

  constructor(
    private storage: Storage
  ) {
    this.storage.get('credentials').then((val) => {
      if (val) {
        this.credentials = val;
        this.createCredentialSingle();
        this.createCredentialGroup();
        this.createCredentialStoredList();
      }
      this.start = true;
    });
  }

  updateCredentials(index:number) {
    this.storage.set('credentials', this.credentials);
    this.updateCredentialStoredList(index);
    if (this.credentials[index]['content']['group']){
      this.updateCredentialGroup(this.credentials[index]);
    }
    else this.updateCredentialSingle(this.credentials[index]);
  }

  addCredentials(credential: object) {
    this.credentials.push(credential);
    this.storage.set('credentials', this.credentials);
    this.addCredentialStoredList(credential['stored']);
    if (credential['content']['group']){
      this.addCredentialGroup(credential);
    }
    else this.addCredentialSingle(credential);
  }

  deleteCredential() {
    if (this.credentials[this.credentialDetailIndex]['content']['group']){
      this.deleteCredentialGroup(this.credentials[this.credentialDetailIndex]);
    }
    else{
      this.deleteCredentialSingle(this.credentials[this.credentialDetailIndex]);
    }
    this.credentials.splice(this.credentialDetailIndex, 1);
    this.deleteCredentialStoredList(this.credentialDetailIndex);
    this.storage.set('credentials', this.credentials);
  }

  getCredentials() {
    return this.credentials;
  }

  getCredentialById(id) {
    let tmpIds = this.credentials.map(elem => elem.id)
    console.log(tmpIds);


    let elementIndex = tmpIds.indexOf(id)
    console.log(elementIndex)
    return elementIndex
  }

  currentData(data) {
    this.currentCredential = data
  }

  createCredentialSingle(){
    console.log("create credential singleeeeeeeeeeeeeeee");
    for (let i=0;i<this.credentials.length;i++){
      if (!this.credentials[i]['content']['group']){
        this.credentialSingle.push(this.credentials[i]);
      }
    }
  }

  updateCredentialSingle(credential:object){
    console.log("update credential singleeeeeeeeeeeeeeee");
    for (let i=0;i<this.credentialSingle.length;i++){
      if (this.credentialSingle[i]['content']['id'] == credential['content']['id']){
        this.credentialSingle[i] = credential;
        break;
      }
    }
  }

  addCredentialSingle(credential:object){
    console.log("add credential singleeeeeeeeeeeeeeee");
    this.credentialSingle.push(credential);
  }

  deleteCredentialSingle(credential:object){
    console.log("delete credential singleeeeeeeeeeeeeeee");
    for (let i=0;i<this.credentialSingle.length;i++){
      if (this.credentialSingle[i]['content']['id'] == credential['content']['id']){
        this.credentialSingle.splice(i, 1);
        break;
      }
    }
  }

  createCredentialGroup(){
    console.log("create credential groupppppppppppppp");
    for (let i=0;i<this.credentials.length;i++){
      if (this.credentials[i]['content']['group']){
        let name = this.credentials[i]['content']['group'];
        let index = this.findGroupName(name);
        if (index > -1){
          this.credentialGroup[index]['list'].push(this.credentials[i]);
        }
        else{
          let newGroup = {
            'group':name,
            'list':[this.credentials[i]]
          }
          this.credentialGroup.push(newGroup);
        }
        
      }
    }
  }

  addCredentialGroup(credential:object){
    console.log("add credential groupppppppppppppp");
    const group = this.findGroupName(credential['content']['group']);
    if (group > -1){
      this.credentialGroup[group]['list'].push(credential);
    }
    else{
      let newGroup = {
        'group':credential['content']['group'],
        'list':[credential]
      }
      this.credentialGroup.push(newGroup);
    }
  }

  updateCredentialGroup(credential:object){
    console.log("update credential groupppppppppppppp");
    for (let i=0;i<this.credentialGroup.length;i++){
      for (let j=0;j<this.credentialGroup[i]['list'].length;j++){
        if (this.credentialGroup[i]['list'][j]['content']['id'] == credential['content']['id']){
          this.credentialGroup[i]['list'][j] = credential;
          return;
        }
      }
    }
  }

  deleteCredentialGroup(credential:object){
    console.log("delete credential groupppppppppppppp");
    for (let i=0;i<this.credentialGroup.length;i++){
      for (let j=0;j<this.credentialGroup[i]['list'].length;j++){
        if (this.credentialGroup[i]['list'][j]['content']['id'] == credential['content']['id']){
          this.credentialGroup[i]['list'].splice(j, 1);
          if (this.credentialGroup[i]['list'].length == 0){
            this.credentialGroup.splice(i, 1);
          }
          return;
        }
      }
    }
  }

  findGroupName(name:string){
    for (let i=0;i<this.credentialGroup.length;i++){
      if (this.credentialGroup[i]['group'] == name){
        console.log("exist group");
        return i;
      }
    }
    console.log("new group");
    return -1
  }

  createCredentialStoredList(){
    for (let i=0;i<this.credentials.length;i++){
      this.credentialStoredList[i] = this.credentials[i]['stored'];
    }
  }

  addCredentialStoredList(credential:CredentialStored){
    this.credentialStoredList.push(credential);
    this.pushCredentialStoredListOnBlockchain();
  }

  deleteCredentialStoredList(index:number){
    this.credentialStoredList.splice(index,1);
    this.pushCredentialStoredListOnBlockchain();
  }

  updateCredentialStoredList(index:number){
    this.credentialStoredList[index] = this.credentials[index]['stored'];
    this.pushCredentialStoredListOnBlockchain();
  }

  async pushCredentialStoredListOnBlockchain(){
    const hash = await this.storedCredentialStoredList();

    const generationHash = await this.getGenerationHash();
    const networkType = await this.getNetworkType();
    const builder = new ModifyAccountMetadataTransactionBuilder();
    const account = Account.createFromPrivateKey(
      StoredAccountService.getPrivateKey(),
      networkType,
    );
    const metadataTx = builder
      .address(account.address)
      .deadline(Deadline.create())
      .generationHash(generationHash)
      .modifications([
        new MetadataModification(
          MetadataModificationType.ADD,
          'credentialStoredListHash',
          hash
        )
      ])
      .networkType(account.address.networkType)
      .feeCalculationStrategy(FeeCalculationStrategy.ZeroFeeCalculationStrategy)
      .build();

    const signedTx = metadataTx.signWith(account, generationHash);

    this.transactionHttp = new TransactionHttp(ApiNodeService.apiNode);
    await this.transactionHttp.announce(signedTx).toPromise();
    
    console.log("hashhhhhhhhhhhhhhhhhkkkkkk: " + signedTx.hash);
    return signedTx.hash;
  }

  async storedCredentialStoredList(): Promise<string>{
    const data = JSON.stringify(this.credentialStoredList);
    const sender = Account.createFromPrivateKey(StoredAccountService.getPrivateKey(),ApiNodeService.NETWORK_TYPE);
    const ipfsConnection = new IpfsConnection(
        CredentialStored.ipfsDomain, // the host or multi address
        CredentialStored.ipfsPort, // the port number
        { protocol: CredentialStored.ipfsProtocol } // the optional protocol
    );

    // Creates Proximax blockchain network connection
    const apiHost = this.convertApiNode();
    const blockchainConnection = new BlockchainNetworkConnection(
      CredentialStored.convertNetworkType(), // the network type
        apiHost.apiDomain, // the rest api base endpoint
        apiHost.apiPort, // the optional websocket end point
        apiHost.apiProtocol
    );

    // Connection Config
    const conectionConfig = ConnectionConfig.createWithLocalIpfsConnection(blockchainConnection, ipfsConnection);

    const uploader = new Uploader(conectionConfig);

    const uploadParamBuilder = UploadParameter.createForStringUpload(data,StoredAccountService.getPrivateKey());
    const uploadParams = uploadParamBuilder
      .withRecipientPublicKey(sender.publicKey)
      .withTransactionMosaics([])
      .withNemKeysPrivacy(StoredAccountService.getPrivateKey(),StoredAccountService.getPublicKey())
      .build();
    return new Promise(resolve => {
        uploader.upload(uploadParams).then(result => {
            console.log("transaction hash: " + result.transactionHash);
            resolve(result.transactionHash);
        });
    })
  }

  async getCredentialStoredListHash(){
    this.metadataHttp = new MetadataHttp(ApiNodeService.apiNode);
    const metadata = await this.metadataHttp
      .getAccountMetadata(StoredAccountService.getAddress())
      .toPromise();

    const hash = metadata.fields.filter(x => x.key === 'credentialStoredListHash')[0].value;

    return hash;
  }

  async getCredentialStoredList(){
    const credentialStoredListHash = await this.getCredentialStoredListHash();
    const ipfsConnection = new IpfsConnection(
        CredentialStored.ipfsDomain, // the host or multi address
        CredentialStored.ipfsPort, // the port number
        { protocol: CredentialStored.ipfsProtocol } // the optional protocol
    );

    // Creates Proximax blockchain network connection
    const apiHost = CredentialStored.convertApiNode();
    const blockchainConnection = new BlockchainNetworkConnection(
        CredentialStored.convertNetworkType(), // the network type
        apiHost.apiDomain, // the rest api base endpoint
        apiHost.apiPort, // the optional websocket end point
        apiHost.apiProtocol
    );

    // Connection Config
    const conectionConfig = ConnectionConfig.createWithLocalIpfsConnection(blockchainConnection, ipfsConnection);

    //
    const dowloader = new Downloader(conectionConfig);

    const downloadParamBuilder = DownloadParameter.create(credentialStoredListHash);
    const downloadParam = downloadParamBuilder.withNemKeysPrivacy(StoredAccountService.getPrivateKey(),StoredAccountService.getPublicKey()).build();
    return new Promise(resolve => {
        dowloader.download(downloadParam).then(async result => {
            const content = await result.data.getContentsAsString();
            console.log("credential list: " + content);
            resolve(JSON.parse(content));
        })
    })
  }

  convertApiNode(){
    let url;
    console.log();
    const lastCharacter = ApiNodeService.apiNode[ApiNodeService.apiNode.length-1];
    if (lastCharacter == "/"){
        url = ApiNodeService.apiNode.slice(0,ApiNodeService.apiNode.length-1);
    }
    else url = ApiNodeService.apiNode;

    const httpsIs = url.indexOf('https');
    let apiProtocol;
    let apiPort;
    let apiDomain;
    if (httpsIs == -1){
        apiProtocol = Protocol.HTTP;
        if (url.indexOf(':') > -1){
            apiDomain = url.slice(7,url.indexOf(':'));
            apiPort = Number(url.slice(url.indexOf(':')+1));
        }
        else {
            apiDomain = url.slice(7);
            apiPort = 80;
        }
    }
    else {
        apiProtocol = Protocol.HTTPS;
        apiPort = 443;
        apiDomain = url.slice(8);
    }
    console.log("=====================");
    console.log(apiProtocol + " " + apiDomain + " " + apiPort);
    console.log("=====================");
    return {
        'apiDomain' : apiDomain,
        'apiPort' : apiPort,
        'apiProtocol' : apiProtocol
    }
  }

  private async getGenerationHash(): Promise<string> {
    this.blockHttp = new BlockHttp(ApiNodeService.apiNode);
    return (await this.blockHttp.getBlockByHeight(1).toPromise())
      .generationHash;
  }

  public async getNetworkType(): Promise<NetworkType> {
    this.networkHttp = new NetworkHttp(ApiNodeService.apiNode);
    return this.networkHttp.getNetworkType().toPromise();
  }
}
