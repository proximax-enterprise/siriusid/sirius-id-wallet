import { TestBed } from '@angular/core/testing';

import { AuthGuardCredentialReceivedService } from './auth-guard-credential-received.service';

describe('AuthGuardCredentialReceivedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthGuardCredentialReceivedService = TestBed.get(AuthGuardCredentialReceivedService);
    expect(service).toBeTruthy();
  });
});
