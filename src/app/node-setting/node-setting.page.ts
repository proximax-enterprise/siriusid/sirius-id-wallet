import { Component, OnInit } from '@angular/core';
import {Storage} from '@ionic/storage';
import {ApiNodeService} from '../../service/api-node.service';
import {NodeHttp, Address} from 'tsjs-xpx-chain-sdk';
import {NotificationService} from 'src/service/notification.service';
import { StoredAccountService } from 'src/service/stored-account.service';
import { element } from 'protractor';
@Component({
  selector: 'app-node-setting',
  templateUrl: './node-setting.page.html',
  styleUrls: ['./node-setting.page.scss'],
})
export class NodeSettingPage implements OnInit {

  nodeList = [];
  otherNodes = [];
  currentNode;
  showBox:boolean = false;
  newNode;
  cssCustom:boolean = false;
  incorrectForm = false;
  constructor(
    private storage:Storage,
    private notificationService: NotificationService  ,
    private storedAccount: StoredAccountService
  ) {}

  ionViewWillEnter(){
    this.nodeList = ApiNodeService.nodeList;
    this.otherNodes = ApiNodeService.otherNodes;
    this.currentNode = ApiNodeService.currentNode;
  }

  async getNodeInfo(node):Promise<boolean>{
    console.log("getNodeInfo");
    let nodeHttp = new NodeHttp(node);

    return new Promise((resolve) => {
      nodeHttp.getNodeInfo().subscribe(nodeInfo => {
        resolve(true);
      }, error => {
        resolve(false);
        console.log(error);
      }, () => {
          console.log("Done");
      });
    })
  }

  async setNode(url:string){
    console.log("setNode");
    console.log(url);
    let index1 = this.checkNodeList(url);
    console.log("-----------index1---------");
    console.log(index1);
    let index2 = this.checkOtherNodes(url);
    console.log("-----------index2---------");
    console.log(index2);
    if (index1 > -1){
      this.nodeList[index1].lived = await ApiNodeService.checkNodeLive(url);
    }
    else{
      this.otherNodes[index2].lived = await ApiNodeService.checkNodeLive(url);
    }
    const address = Address.createFromRawAddress(StoredAccountService.address);
    this.storedAccount.checkAccountInfo(address);
    this.notificationService.updateWs();
    this.storage.set("currentNode",this.currentNode);
    this.storage.set("otherNodes",this.otherNodes);
  }

  checkNodeList(url:string){
    let result = -1;
    for (let i=0;i<this.nodeList.length;i++){
      if (this.nodeList[i].url == url){
        this.nodeList[i].chosen = true;

        this.currentNode.index = i;
        this.currentNode.otherNode = false;
        ApiNodeService.nodeList[i].chosen = true;
        ApiNodeService.nodeList[i].lived = this.nodeList[i].lived;
        ApiNodeService.apiNode = this.nodeList[i].url;
        console.log("apiNodeeeeeeeeeee");
        console.log(ApiNodeService.apiNode);
        result = i;
      }
      else {
        this.nodeList[i].chosen = false;
        this.nodeList[i].lived = false;
      }
    }
    return result;
  }

  checkOtherNodes(url:string){
    let result = -1;
    for (let i=0;i<this.otherNodes.length;i++){
      if (this.otherNodes[i].url == url){
        this.otherNodes[i].chosen = true;
        

        this.currentNode.index = i;
        this.currentNode.otherNode = true;
        ApiNodeService.otherNodes[i].chosen = true;
        ApiNodeService.otherNodes[i].lived = this.otherNodes[i].lived;
        ApiNodeService.apiNode = this.otherNodes[i].url;
        console.log("apiNodeeeeeeeeeee");
        console.log(ApiNodeService.apiNode);
        result = i;
      }
      else {
        this.otherNodes[i].chosen = false;
        this.otherNodes[i].lived = false;
      }
    }
    return result;
  }
  async addNode(){
    if(this.checkFormatApiNode()){
      let node = {
        url:this.newNode,
        chosen: true,
        lived: false
      };
      this.newNode = "";
      this.otherNodes.push(node);
      this.setNode(node.url);
      this.boxToggle();
    }
    else this.incorrectForm = true;
    
  }

  boxToggle(){
    this.incorrectForm = false;
    this.newNode = "";
    this.showBox = !this.showBox;
  }

  ionViewDidEnter(){
    this.cssCustom = true;
  }
  ngOnInit() {
  }

  checkFormatApiNode(){
    if ((this.newNode.indexOf('http')) > -1 || (this.newNode.indexOf('https') > -1)){
      return true;
    }
    else return false;
  }

}
