import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactReceivedPage } from './contact-received.page';

describe('ContactReceivedPage', () => {
  let component: ContactReceivedPage;
  let fixture: ComponentFixture<ContactReceivedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactReceivedPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactReceivedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
