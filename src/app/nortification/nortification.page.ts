import { Component, OnInit } from '@angular/core';
import {NotificationService} from '../../service/notification.service';
@Component({
  selector: 'app-nortification',
  templateUrl: './nortification.page.html',
  styleUrls: ['./nortification.page.scss'],
})
export class NortificationPage implements OnInit {
  notification : any;
  alert: any;
  constructor(private notiService: NotificationService) {
    this.notification = this.notiService.notification;
    this.alert = this.notiService.alert;
  }

  ngOnInit() {
  }

  update(){
    console.log(this.notification);
    console.log(this.alert);
    this.notiService.notification = this.notification;
    this.notiService.alert = this.alert;
  }

}
