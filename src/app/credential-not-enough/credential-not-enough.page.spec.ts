import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CredentialNotEnoughPage } from './credential-not-enough.page';

describe('CredentialNotEnoughPage', () => {
  let component: CredentialNotEnoughPage;
  let fixture: ComponentFixture<CredentialNotEnoughPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CredentialNotEnoughPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CredentialNotEnoughPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
