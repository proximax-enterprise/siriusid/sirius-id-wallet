import { Component, OnInit } from '@angular/core';
import { MessageType } from 'siriusid-sdk';
import { DataForConfirmService } from '../../service/data-for-confirm.service';
import { GetCoinNameService } from '../../service/get-coin-name.service';
import { Address, TransactionType, NetworkType } from 'tsjs-xpx-chain-sdk';
import { AuthGuardConfirmService } from '../../service/auth-guard-confirm.service';
import { Router } from '@angular/router';
import { CredentialsService } from 'src/service/credentials.service';
import { ApiNodeService } from '../../service/api-node.service';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.page.html',
  styleUrls: ['./confirm.page.scss'],
})
export class ConfirmPage implements OnInit {
  MESSAGE_TYPE; // save type of message received
  ADDRESS; // addres of dApp, ERROR: currently it's public key
  MESSAGE;
  mosaics = [];
  transaction;

  constructor(
    private dataForConfirmService: DataForConfirmService,
    private auth: AuthGuardConfirmService,
    private router: Router,
    public credentialsService: CredentialsService
  ) {
    this.transaction = this.dataForConfirmService.transaction;
  }

  async ngOnInit() {
    const messageReceived = this.dataForConfirmService.messageReceived;
    console.log(messageReceived.message);
    this.MESSAGE_TYPE = messageReceived.message.type;
    this.dataForConfirmService.messageType = this.MESSAGE_TYPE;

    switch (this.MESSAGE_TYPE){
      case MessageType.LOG_IN:
        this.loginAndVerifyMessage(messageReceived);
        break;
      case MessageType.TRANSACTION:
        await this.transactionMessage();
        break;
      case MessageType.VERIFY_BOTH:
        this.loginAndVerifyMessage(messageReceived);
        break;
      case MessageType.VERIFY_HARD:
        this.loginAndVerifyMessage(messageReceived);
        break;
      case MessageType.VERIFY_LIGHT:
        this.loginAndVerifyMessage(messageReceived);
        break;
    }
  }

  result() {
    this.router.navigate(['/confirm4']);
    this.auth.auth = false;
  }

  loginAndVerifyMessage(messageReceived){
    this.ADDRESS = Address.createFromPublicKey(messageReceived.message.payload.appPublicKey, ApiNodeService.NETWORK_TYPE).plain();
    console.log('this.ADDRESS ---------------->', this.ADDRESS)
  }

  async transactionMessage(){
    console.log('transaction Type');
    const transaction = this.dataForConfirmService.transaction;
    if (transaction.type === TransactionType.TRANSFER) {// just apply for transfer transaction
      console.log('transfer');
      this.ADDRESS = transaction.recipient.pretty();
      this.MESSAGE = transaction.message.payload;
      for (let i = 0; i < transaction.mosaics.length; i++) {
        console.log('array mosaic');
        const mosaic = Object();
        mosaic.coin_name = await GetCoinNameService.getCoinName(transaction.mosaics[i].id);
        mosaic.amount = transaction.mosaics[i].amount.compact() / 1000000;
        this.mosaics[i] = mosaic;
      }
      console.log(this.mosaics);
    }
  }
}
