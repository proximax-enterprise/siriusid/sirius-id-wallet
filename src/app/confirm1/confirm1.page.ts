import { Component, OnInit } from '@angular/core';
import {DataForConfirmService} from '../../service/data-for-confirm.service'
import {AuthGuardConfirm1Service} from '../../service/auth-guard-confirm1.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-confirm1',
  templateUrl: './confirm1.page.html',
  styleUrls: ['./confirm1.page.scss'],
})
export class Confirm1Page implements OnInit {
  loading;
  successTx;
  sender;
  MESSAGE_TYPE;
  transaction;
  constructor(
    private dataForConfirmService: DataForConfirmService,
    private auth: AuthGuardConfirm1Service,
    private router: Router,
    ) { }
  
  ngOnInit() {
    var messageReceived = this.dataForConfirmService.messageReceived;

    this.MESSAGE_TYPE = messageReceived.message.type;
    this.dataForConfirmService.messageType = this.MESSAGE_TYPE;

    console.log(this.MESSAGE_TYPE);
  }

  result(){
    this.auth.auth = false;
    this.router.navigate(['/confirm4']);
    
  }
}
