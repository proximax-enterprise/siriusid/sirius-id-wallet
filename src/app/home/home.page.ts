import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { StoredInfoService } from '../../service/stored-info.service';
import { StoredAccountService } from '../../service/stored-account.service';
import { ApiNodeService } from '../../service/api-node.service';
import { CredentialsService } from '../../service/credentials.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})

export class HomePage implements OnInit {
  constructor(
    private router: Router,
    private storage: Storage,
    private apiNodeService: ApiNodeService,
    // private storedInfo: StoredInfoService,
    // private storedAccount: StoredAccountService,
    // private credentialService: CredentialsService
  ) {
    console.log('HomePage');
  }

  ngOnInit() {}

  goToRecovery() {
    console.log('Holaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
    this.storage.set('goToRecovery', true);

    this.router.navigate(['/recovery']);
  }
}
