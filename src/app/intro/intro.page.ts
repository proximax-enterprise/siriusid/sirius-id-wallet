import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss']
})

export class IntroPage implements OnInit {

  slideOpts = {
    initialSlide: 0,
    speed: 400,
    pagination: {
      el: '.swiper-pagination',
    }
  };

  constructor(private router: Router) {
  }

  navigate() {
    this.router.navigate(['/signup']);
  }

  ngOnInit() {
  }
}
