import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import {Clipboard} from '@ionic-native/clipboard/ngx';
import {StoredAccountService} from 'src/service/stored-account.service';
import {StoredInfoService} from 'src/service/stored-info.service';
import { ApiNodeService } from 'src/service/api-node.service';
@Component({
  selector: 'app-my-qrcode',
  templateUrl: './my-qrcode.page.html',
  styleUrls: ['./my-qrcode.page.scss'],
})
export class MyQRcodePage implements OnInit {

  name:string;
  address:string;
  networkType: String;
  qr:string;
  elementType: 'url' | 'canvas' | 'img' = 'canvas';
  copy:boolean = false;
  balance;

  constructor(
    private storage: Storage,
    private cliboard: Clipboard,
    private storedInfo: StoredInfoService,
    private apiNodeService: ApiNodeService

  ) {
    console.log("kkkkkkkkkkkkkkkkk");
    this.name = storedInfo.getName();
    this.address = StoredAccountService.getAddress();
    this.networkType = this.networkTypeString();
    this.balance = StoredAccountService.balance;
    console.log("hhhhhhhhhhhhhhhhhh");
  }

  ngOnInit() {
  }

  copyText(){
    this.cliboard.copy(this.address);
    this.copy = true;
  }

  ionViewDidLeave(){
    this.copy = false;
  }

  networkTypeString(){
    switch (ApiNodeService.NETWORK_TYPE){
      case 184:
        return "MAIN_NET";
      case 168:
          return "TEST_NET";
      case 200:
        return "PRIVATE";
      case 176:
        return "PRIVATE_TEST";
      case 96:
        return "MIJIN";
      case 144:
        return "MIJIN_TEST";
      default : return "TEST_NET";
    }
  }
}
