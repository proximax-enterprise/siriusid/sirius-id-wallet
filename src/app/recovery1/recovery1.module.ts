import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Recovery1Page } from './recovery1.page';
import { SharedModule } from '../shared/shared.module';


const routes: Routes = [
  {
    path: '',
    component: Recovery1Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [Recovery1Page]
})
export class Recovery1PageModule {
}
