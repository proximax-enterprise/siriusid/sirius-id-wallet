import { Component, OnInit } from '@angular/core';
import { StoredAccountService } from '../../service/stored-account.service';
import { StoredInfoService } from '../../service/stored-info.service';
import {SiriusDidService} from 'src/service/sirius-did.service'; 
import { Storage } from '@ionic/storage';
import { ApiNodeService } from 'src/service/api-node.service';
import { SiriusChainProvider } from 'tsjs-did-siriusid';
import { HttpClient } from '@angular/common/http';
import { CredentialsService } from 'src/service/credentials.service';
import { CredentialStored } from 'siriusid-sdk';
@Component({
  selector: 'app-recovery1',
  templateUrl: './recovery1.page.html',
  styleUrls: ['./recovery1.page.scss'],
})
export class Recovery1Page implements OnInit {

  progress = 0;
  btnNext = false;
  constructor(
    private storage: Storage, 
    private storedInfo: StoredInfoService, 
    private http: HttpClient,
    private siriusDidService: SiriusDidService,
    private credentialService: CredentialsService
  ) {}

  async ngOnInit() {
    this.siriusDidService.initialize = true;
    console.log("Recovery flow - checking didDocument");
    console.log(ApiNodeService.apiNode);
    SiriusDidService.siriusChainProvider = ApiNodeService.apiNode;
    SiriusDidService.registryProvider = new SiriusChainProvider(
      SiriusDidService.siriusChainProvider
    );
    
    this.storage.set('privateKey', StoredAccountService.getPrivateKey());
    this.storage.set('publicKey', StoredAccountService.getPublicKey());
    this.storage.set('address', StoredAccountService.getAddress());

    const interval = setInterval(() => {
      
      if (this.progress <= 0.65 && !this.btnNext) {
        this.progress += 0.15;
      }
      else if (this.progress <= 0.9 && this.progress > 0.65 && !this.btnNext) {
        this.progress += 0.1;
        
      }
      else if (this.btnNext){
        this.progress = 1;
        clearInterval(interval);
      }
    }, 150);

    const didExist = await SiriusDidService.checkDidExist();
    if(didExist){
      console.log("Recovery flow - didDocument exist");
      const didDocument = await SiriusDidService.getDid();
      const endpoint = didDocument.toJSON().services[0].serviceEndpoint;
      this.getPubProfile(endpoint);
    }
    else{
      await SiriusDidService.createNewDid();
    }

    const hash = await this.credentialService.getCredentialStoredListHash();
    if (hash){
      const listCredentialHash = <any>await this.credentialService.getCredentialStoredList();
      for (let i=0;i<listCredentialHash.length;i++){
        let item = new CredentialStored(listCredentialHash[i]['keyDecrypt'],listCredentialHash[i]['credentialHash']);
        let credentialContent = await item.getCredential(item.getCredentialHash(),item.getKeyDecrypt(),StoredAccountService.getPublicKey());
        const credential = {
          'content': credentialContent,
          'stored': item
        }
        this.credentialService.credentials.push(credential);
        this.storage.set('credentials', this.credentialService.credentials);
      }
      this.credentialService.createCredentialSingle();
      this.credentialService.createCredentialGroup();
      this.credentialService.createCredentialStoredList();
      this.btnNext = true;
    }
    else{
      this.btnNext = true;
    }
  }

  getPubProfile(endPoint: string){
    const url = endPoint.replace(':5443','');
    return this.http.get(url, {responseType: 'json'}).subscribe(response => {
      console.log("Public profile is:",response);
      this.storedInfo.setName(response['name']);
      this.storedInfo.setEmail(response['email']);
      this.storedInfo.setAvatar(response['avatar']);
      this.storedInfo.setCountry(response['country']);
      this.storedInfo.setPhoneNumber(response['phoneNumber']);

      this.setUserProfile();
    })
  }

  setUserProfile(){
    this.storage.set('name', this.storedInfo.getName());
    this.storage.set('email', this.storedInfo.getEmail());
    this.storage.set('avatar', this.storedInfo.getAvatar());
    this.storage.set('country', this.storedInfo.getCountry());
    this.storage.set('phoneNumber', this.storedInfo.getPhoneNumber());
    this.btnNext = true;
  }
}
