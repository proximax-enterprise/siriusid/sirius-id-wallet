import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { Account } from 'tsjs-xpx-chain-sdk';
import { Storage } from '@ionic/storage';
import { StoredAccountService } from '../../service/stored-account.service';
import { ApiNodeService } from '../../service/api-node.service';
@Component({
  selector: 'app-signup1',
  templateUrl: './signup1.page.html',
  styleUrls: ['./signup1.page.scss']
})
export class Signup1Page implements OnInit {
  progress = 0;
  btnNext = false;
  account: Account;
  readonly networkType = ApiNodeService.NETWORK_TYPE;

  constructor(
    private router: Router, 
    private storage: Storage,
    private storedAccountService: StoredAccountService,
  ) {
    this.account = Account.generateNewAccount(this.networkType);
    const interval = setInterval(() => {
      this.progress += 0.1;
      if (this.progress >= 1) {
        this.btnNext = true;
        clearInterval(interval);
      }
    }, 150);
  }

  navigate() {
    this.router.navigate(['/signup2']);
  }

  storedAccount() {
    StoredAccountService.setPrivateKey(this.account.privateKey);
    StoredAccountService.setPublicKey(this.account.publicKey);
    StoredAccountService.setAddress(this.account.address.plain());
    this.storage.set('privateKey', this.account.privateKey);
    this.storage.set('publicKey', this.account.publicKey);
    this.storage.set('address', this.account.address.plain());
    this.navigate();
  }
  ngOnInit() {
  }

}
