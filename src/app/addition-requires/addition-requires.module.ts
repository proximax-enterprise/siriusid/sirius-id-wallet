import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdditionRequiresPageRoutingModule } from './addition-requires-routing.module';

import { AdditionRequiresPage } from './addition-requires.page';
import { Base64 } from '@ionic-native/base64/ngx';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
const routes: Routes = [
  {
    path: '',
    component: AdditionRequiresPage
  }
];
@NgModule({
  imports: [
    // CommonModule,
    // FormsModule,
    // IonicModule,
    // AdditionRequiresPageRoutingModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  providers:[
    Base64
  ],
  declarations: [AdditionRequiresPage]
})
export class AdditionRequiresPageModule {}
