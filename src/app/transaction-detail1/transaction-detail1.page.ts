import { Component, OnInit } from '@angular/core';
import {HistoryService} from '../../service/history.service';

@Component({
  selector: 'app-transaction-detail1',
  templateUrl: './transaction-detail1.page.html',
  styleUrls: ['./transaction-detail1.page.scss'],
})
export class TransactionDetail1Page implements OnInit {

  type: any;
  recepient: any;
  signer: any;
  dateTime: any;
  i: any;
  mosaic: any;
  message: any;
  coinName: any;

  constructor(private historyService: HistoryService) { 
    this.i = this.historyService.i;
    this.recepient = this.historyService.recepient[this.i];
    this.signer = this.historyService.signer[this.i];
    this.dateTime = this.historyService.dateTime[this.i];
    this.type = this.historyService.type[this.i];
    this.mosaic = this.historyService.mosaic[this.i];
    this.message = this.historyService.message[this.i];
    this.coinName = this.historyService.coinName[this.i];
  }

  ngOnInit() {
  }

}
