import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/service/notification.service';
import { Router } from '@angular/router';
import { AuthGuardCredentialConfirmService } from 'src/service/auth-guard-credential-confirm.service';
import {CredentialResponseMessage, Credentials} from 'siriusid-sdk';
import {StoredAccountService} from 'src/service/stored-account.service';
import { PublicAccount, EncryptedMessage, TransferTransaction, Deadline, UInt64 } from 'tsjs-xpx-chain-sdk';
import { ApiNodeService } from 'src/service/api-node.service';
@Component({
  selector: 'app-credential-confirm',
  templateUrl: './credential-confirm.page.html',
  styleUrls: ['./credential-confirm.page.scss'],
})
export class CredentialConfirmPage implements OnInit {

  payload;
  constructor(
    public notificationService: NotificationService,
    private router: Router,
    private auth: AuthGuardCredentialConfirmService
  ) { 
    this.payload = this.notificationService.listNotifications.pop();
    this.notificationService.updateListNotification();
    this.notificationService.updateNumberNoti();
  }

  cancelBtn(){
    this.notificationService.listNotifications.pop();
    this.notificationService.updateListNotification();
    this.notificationService.updateNumberNoti();
    this.router.navigate(['/tabs/notification']);
  }

  confirmBtn(){
    console.log("TO DO");
    this.notificationService.confirmCredential = true;
    this.createTransaction();
    this.auth.auth = false;
    this.router.navigate(['/confirm4']);
  }

  ngOnInit() {
  }

  createTransaction(){
    let credential = Credentials.create(
      this.payload.credential.id,
      this.payload.credential.name,
      this.payload.credential.description,
      this.payload.credential.icon,
      this.payload.credential.documentHash,
      this.payload.credential.content,
      this.payload.credential.auth_origin,
    );
    console.log(credential);
    credential.addAuthOwner(Credentials.authCreate(credential.getContent(),StoredAccountService.getPrivateKey()));
    let mess = CredentialResponseMessage.create(
      this.payload.appPublicKey,
      this.payload.sessionToken,
      credential
    );
    console.log(mess);
    let pubAcc = PublicAccount.createFromPublicKey(this.payload.appPublicKey,ApiNodeService.NETWORK_TYPE);
    let encMess = EncryptedMessage.create(mess.toString(),pubAcc,StoredAccountService.getPrivateKey());

    this.notificationService.transaction = TransferTransaction.create(
      Deadline.create(),
      pubAcc.address,
      [],
      encMess,
      ApiNodeService.NETWORK_TYPE,
      new UInt64([0,0])
    );
  }
}
